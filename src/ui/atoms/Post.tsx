import { IPost } from "../../interfaces/Post";
import TimeAgo from "timeago-react";
import FavoriteIcon from "./FavoriteIcon";
import timeImg from "../../assets/images/time.svg";

interface IPostProps {
  post: IPost;
  handleFavorite: (id: string) => void;
  isFavorite?: boolean;
}

const Post = ({ post, handleFavorite, isFavorite = false }: IPostProps) => {

  const handleClick = (ev: any) => {
    ev.preventDefault();
    handleFavorite(post.objectID);
  };

  return (
    <a href={post.story_url} target="_blank" rel="noreferrer" className="posts__item">
      <main>
        <div className="information">
          <img
            src={timeImg}
            alt="time icon"
            className="time-icon"
            height="16"
          />
          <span>
            <TimeAgo datetime={post.created_at} /> by {post.author}
          </span>
        </div>
        <div className="content">{post.story_title}</div>
      </main>
      <aside >
        <FavoriteIcon active={isFavorite} onClick={handleClick} />
      </aside>
    </a>
  );
};

export default Post;
