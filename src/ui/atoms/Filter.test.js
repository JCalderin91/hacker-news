import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render } from "@testing-library/react";

import Filter from './Filter';

describe("Given a vale", () => {
  describe("When the value is true", () => {
    test("Then I should work as expected", () => {
      const component = render(<Filter value={true} />)
      const all = component.queryByText(/all/i)
      expect(all).toBeInTheDocument()
      expect(all).toHaveClass("filter__item--active")

      const myFaves = component.queryByText(/my faves/i)
      expect(myFaves).toBeInTheDocument()
      expect(myFaves).not.toHaveClass("filter__item--active")
    })
  })
  test("Then the All button click event should work as expected", () => {
    const mockClick = jest.fn()
    const component = render(<Filter value={true} toogleFilter={mockClick} />)
    const btnAll = component.queryByText(/all/i)
    fireEvent.click(btnAll)
    expect(mockClick).toBeCalledTimes(1)
    expect(mockClick).toBeCalledWith(true)
  })
  test("Then the My faves button click event should work as expected", () => {
    const mockClick = jest.fn()
    const component = render(<Filter value={true} toogleFilter={mockClick} />)
    const btnMyFavs = component.queryByText(/My faves/i)
    fireEvent.click(btnMyFavs)
    expect(mockClick).toBeCalledTimes(1)
    expect(mockClick).toBeCalledWith(false)
  })
})


