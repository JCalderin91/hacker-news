
import favIconActive from "../../assets/images/favorite-active.svg";
import favIcon from "../../assets/images/favorite-inactive.svg";

interface IFavoriteIcon {
  active: boolean;
  onClick: (ev:any) => void;
}

const FavoriteIcon = ({ active, ...attrs }: IFavoriteIcon) => {
  return (
    <img
      src={active ? favIconActive : favIcon}
      height="24"
      width="22"
      className={`favorite-icon ${active ? "active" : ""}`}
      alt={`heart-icon ${active ? "favorite" : ""}`}
      {...attrs}
    />
  );
};

export default FavoriteIcon;
