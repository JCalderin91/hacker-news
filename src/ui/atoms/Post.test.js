import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render } from "@testing-library/react";

import Post from './Post';

const post = {
  objectID: "1",
  created_at: new Date(),
  story_title: "Story Title",
  story_url: "https://www.google.com",
  author: "John Doe",
}
describe("Given a post", () => {
  test("Then I see the title and author of the post", () => {
    const component = render(<Post post={post} />)
    expect(component.container).toHaveTextContent(post.story_title);
    expect(component.container).toHaveTextContent(post.author);
  })
  test("Then the button click event should work as expected", () => {
    const mockFn = jest.fn();
    const component = render(<Post post={post} handleFavorite={mockFn} />)
    const button = component.getByAltText("heart-icon")
    fireEvent.click(button)
    expect(mockFn).toHaveBeenCalledTimes(1)
  })
  test("Then I see the publish time as Just now", () => {
    const component = render(<Post post={post} />)
    expect(component.container).toHaveTextContent(/just now/i)
  })
})


