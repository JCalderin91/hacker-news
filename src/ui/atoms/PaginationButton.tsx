import React from "react";

interface IPaginationButtonProps {
  active: boolean;
  children: React.ReactNode;
  onClick: () => void;
}

const PaginationButton = ({ active, children, ...attrs }: IPaginationButtonProps) => {
  return (
    <button
      className={`pagination__item ${active ? "pagination__item--active" : ""}`}
      {...attrs}
    >
      {children}
    </button>
  );
};

export default PaginationButton;
