
import hackerNewsLogo from "../../assets/images/hacker-news.svg";

const Header = () => {
  return (
    <header className="main-header">
      <div className="container">
        <img src={hackerNewsLogo} height="28" width="208" alt="hacker news logo" />
      </div>
    </header>
  );
};

export default Header;
