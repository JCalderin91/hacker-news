import { components } from 'react-select'

const CustomValueContainer = ({ children, ...props }) => {
  const src = props.getValue()[0].image
  return (
    <components.ValueContainer {...props}>
      <div className="custom-value-container">
        <img src={src} height="24" alt={src}></img> {children}
      </div>
    </components.ValueContainer>
  )
};

export default CustomValueContainer