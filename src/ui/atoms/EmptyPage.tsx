

const EmptyPage = ({ text }: { text: string }) => {
  return <p className="text-center">{text}</p>;
}
export default EmptyPage;