import CustomOption from "./CustomOption";
import Select from 'react-select'
import CustomValueContainer from "./CustomValueContainer";

const CustomSelect = ({ options, defaultValue, onChange }) => {
  return <Select options={options} defaultValue={defaultValue} onChange={onChange} components={{ Option: CustomOption, ValueContainer: CustomValueContainer }} />
};

export default CustomSelect;
