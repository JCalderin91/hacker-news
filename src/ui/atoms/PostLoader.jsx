import ContentLoader from "react-content-loader"

const PostLoader = (props) => (
    <ContentLoader
        speed={3}
        width={460}
        height={82}
        viewBox="0 0 455 82"
        backgroundColor="#f2f2f2"
        foregroundColor="#dedede"
        style={{ width: '100%' }}
        {...props}
    >
        <rect x="35" y="41" rx="3" ry="3" width="88" height="6" />
        <rect x="15" y="71" rx="3" ry="3" width="380" height="6" />
        <circle cx="19" cy="43" r="8" />
        <rect x="14" y="57" rx="3" ry="3" width="380" height="6" />
        <circle cx="422" cy="57" r="13" />
    </ContentLoader>
)

export default PostLoader