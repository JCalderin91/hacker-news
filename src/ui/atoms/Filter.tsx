
interface IFilterProps {
  value: boolean;
  toogleFilter: (value: boolean) => void;
}

const Filter = ({ value, toogleFilter }: IFilterProps) => {
  return (
    <div className="filter">
      <div
        className={`filter__item ${value ? "filter__item--active" : ""}`}
        onClick={() => {
          toogleFilter(true);
        }}
      >
        All
      </div>
      <div
        className={`filter__item ${!value ? "filter__item--active" : ""}`}
        onClick={() => {
          toogleFilter(false);
        }}
      >
        My faves
      </div>
    </div>
  );
};

export default Filter;
