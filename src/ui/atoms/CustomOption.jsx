import { components } from 'react-select'

const CustomOption = ({ children, image, ...props }) => {
  return (
    <components.Option {...props}>
      <div className="custom-option">
        <img src={props.data.image} height="24" alt={props.data.image}></img> {children}
      </div>
    </components.Option>
  )
};

export default CustomOption