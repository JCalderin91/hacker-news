
import Header from "../atoms/Header";
import Filter from "../atoms/Filter";
import Pagination from "../molecules/Pagination";
import PostList from "../molecules/PostList";
import CustomSelect from "../atoms/CustomSelect";

import { usePosts } from "../../hooks/usePosts";
import frameworks from "../../data/frameworks";


const Home = () => {
  const {
    posts,
    pageCount,
    filterAll,
    favorites,
    page,
    query,
    isLoading,
    handleFavorite,
    toogleFilter,
    changeTypeFilter,
    handleChangePage
  } = usePosts()

  return (
    <div className="Home">
      <Header />
      <main className="container">
        <div className="main-filter">
          <Filter value={filterAll} toogleFilter={toogleFilter} />
        </div>

        <div className="select">
          <div className="type-filter">
            {filterAll && <CustomSelect options={frameworks} defaultValue={query} onChange={changeTypeFilter} />}
          </div>

          <PostList isLoading={isLoading} posts={posts} handleFavorite={handleFavorite} filterAll={filterAll} favorites={favorites} />

          <Pagination
            page={page}
            max={pageCount}
            handleChangePage={handleChangePage}
          />
        </div>
      </main>
    </div>
  );
};

export default Home;
