import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";

import Home from './Home';


describe("When the app init", () => {
  test("Then I should see the home page", () => {
    const component = render(<Home />);
    expect(component.container).toBeInTheDocument();
  })
})


