import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render } from "@testing-library/react";

import Pagination from './Pagination';

describe("Given a page", () => {
  describe("When the max page is 0", () => {
    test("Then I see the 1 number", () => {
      const component = render(<Pagination page={0} max={1} />)
      expect(component.container).toHaveTextContent(1);
    })
    test("Then I see only the button of the page one", () => {
      const component = render(<Pagination page={0} max={1} />)
      const buttons = component.queryAllByRole("button")
      expect(buttons).toHaveLength(1)
    })
  })
  describe("When the max page is 2 and the page is 0", () => {
    test("Then I see two page buttons and next page button", () => {
      const component = render(<Pagination page={0} max={2} />)
      const buttons = component.queryAllByRole("button")
      expect(buttons).toHaveLength(3)
    })
  })
  describe("When the max page is 3 and the page is 1", () => {
    test("Then I see three page buttons and the next and prev buttons", () => {
      const component = render(<Pagination page={1} max={3} />)
      const buttons = component.queryAllByRole("button")
      const prevBtn = component.queryByAltText("arrow-left")
      const nextBtn = component.queryByAltText("arrow-right")
      expect(buttons).toHaveLength(5)
      expect(prevBtn).toBeInTheDocument()
      expect(nextBtn).toBeInTheDocument()
    })
  })
  describe("When the buttons are present", () => {
    test("Then clicking the page button should work as expected", () => {
      const mockClick = jest.fn();
      const component = render(<Pagination page={2} max={10} handleChangePage={mockClick} />)
      const pageButton = component.getByText("3");
      fireEvent.click(pageButton);
      expect(mockClick).toBeCalledWith(2);
    })
    test("Then clicking the prev button should work as expected", () => {
      const mockClick = jest.fn();
      const component = render(<Pagination page={2} max={10} handleChangePage={mockClick} />)
      const leftButton = component.getByAltText("arrow-left");
      fireEvent.click(leftButton);
      expect(mockClick).toHaveBeenCalledTimes(1);
    })
    test("Then clicking the next button should work as expected", () => {
      const mockClick = jest.fn();
      const component = render(<Pagination page={2} max={10} handleChangePage={mockClick} />)
      const rightButton = component.getByAltText("arrow-right");
      fireEvent.click(rightButton);
      expect(mockClick).toHaveBeenCalledTimes(1);
    })
  })
})



