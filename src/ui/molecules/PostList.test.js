import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";

import PostList from './PostList';

const posts = [{
  objectID: "1",
  created_at: "2020-01-01",
  story_title: "Story Title",
  story_url: "https://www.google.com",
  author: "John Doe",
},
{
  objectID: "2",
  created_at: "2020-01-02",
  story_title: "Second Story Title",
  story_url: "https://www.google.com",
  author: "Mr. Max",
}]

describe("Given a list of posts", () => {
  describe("When the filter is true", () => {
    test("Then I expect see the post title", () => {
      const component = render(<PostList posts={posts} favorites={{}} filterAll={true} />)
      expect(component.container).toHaveTextContent(posts[0].story_title);
    })
  })
  describe("When there are favorite posts and the filter is true", () => {
    test("Then I expect see one favorite", async () => {
      const favorites = { [posts[0].objectID]: true }
      const component = render(<PostList posts={posts} favorites={favorites} filterAll={true} />)
      const favoriteButtons = await component.queryAllByAltText("heart-icon favorite");
      expect(favoriteButtons).toHaveLength(1);
    })
  })
  describe("When there are favorites post and the filter is false", () => {
    test("Then I expect see only favorites post", async () => {
      const favorites = { [posts[0].objectID]: true }
      const component = render(<PostList posts={posts} favorites={favorites} filterAll={false} />)
      const favoriteButtons = await component.queryAllByAltText(/heart-icon/);
      expect(favoriteButtons).toHaveLength(1);
    })
  })

  describe("When there are not favorites post and the filter is false", () => {
    test("Then I see the message of not having favorite posts", () => {
      const component = render(<PostList posts={posts} favorites={{}} filterAll={false} />)
      expect(component.container).toHaveTextContent("You do not have favorites on this page");
    })
  })

})


describe("Given a list of posts is empty", () => {
  test("Then I see a empty list message", () => {
    const component = render(<PostList posts={[]} favorites={{}} filterAll={true} />)
    expect(component.container).toHaveTextContent("The results of the current page do not comply with the format required to be displayed");
  })
})

