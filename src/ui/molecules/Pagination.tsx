import PaginationButton from "../atoms/PaginationButton";
import { useResize } from "../../hooks/useResize";
import chevronLeftSvg from "../../assets/images/chevron-left.svg"
import chevronRightSvg from "../../assets/images/chevron-right.svg"

interface PaginationProps {
  page: number,
  min?: number,
  max?: number,
  handleChangePage: (page: number) => void
}

const Pagination = ({ page, min = 0, max = 0, handleChangePage }: PaginationProps) => {
  const { offsetPages } = useResize({ maxWidth: 768 });
  const offsetMax = (offsetPages * 2) + 1

  const handlePagination = (value: number) => {
    if (value >= min && value < max) handleChangePage(value);
  };
  let prevItems: number[] = [];
  for (let index = page - 1; index >= 0; index--) {
    prevItems = [index, ...prevItems];
  }

  let nextItems: number[] = [];
  for (let index = page; index < max; index++) {
    nextItems = [...nextItems, index];
  }

  const nextItemLength = nextItems.length;
  nextItems = nextItems.slice(0, offsetMax - prevItems.slice(-offsetPages).length);
  if (nextItemLength <= offsetPages) prevItems = prevItems.slice(-offsetMax + nextItemLength);
  else prevItems = prevItems.slice(-offsetPages);

  const pagesItems = [...prevItems, ...nextItems];

  return (
    <section className="pagination">
      {page !== 0 && <PaginationButton active={false} onClick={() => handlePagination(page - 1)} >
        <img src={chevronLeftSvg} alt="arrow-left" />
      </PaginationButton>}

      {pagesItems.map((number) => (
        <PaginationButton
          active={page === number}
          key={number}
          onClick={() => handlePagination(number)}
        >{number + 1}</PaginationButton>
      ))}

      {page < max - 1 && <PaginationButton active={false} onClick={() => handlePagination(page + 1)} >
        <img src={chevronRightSvg} alt="arrow-right" />
      </PaginationButton>}
    </section>
  );
};

export default Pagination;
