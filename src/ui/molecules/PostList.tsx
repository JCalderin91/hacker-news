import { IFavorite } from "../../interfaces/Favorite";
import { IPost } from "../../interfaces/Post";
import EmptyPage from "../atoms/EmptyPage";
import Post from "../atoms/Post";
import PostLoader from "../atoms/PostLoader";

interface PostListProps {
  posts: IPost[];
  handleFavorite: (id: string) => void;
  isLoading: boolean;
  filterAll: boolean;
  favorites: IFavorite;
}

const PostList = ({ isLoading, posts, favorites, handleFavorite, filterAll }: PostListProps) => {
  if (!posts.length)
    return <EmptyPage text="The results of the current page do not comply with the format required to be displayed" />;

  if (!filterAll) {
    posts = posts.filter(({ objectID }) => favorites[objectID])
    if (!posts.length)
      return <EmptyPage text="You do not have favorites on this page" />;
  }
  if (isLoading) {
    return <section className="posts">
      <PostLoader />
      <PostLoader />
      <PostLoader />
      <PostLoader />
    </section>
  }
  return <section className="posts">
    {posts.map((post: IPost, key) => (
      <Post
        post={post}
        key={key}
        handleFavorite={handleFavorite}
        isFavorite={!!favorites[post.objectID]}
      />
    ))}
  </section>

}
export default PostList;