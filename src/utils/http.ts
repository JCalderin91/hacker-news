export const http = {
  get(url: string): Promise<any> {
    return fetch(url)
      .then(data => data.json())
      .then(response => response)
      .catch(error => {
        throw error
      })
  }
}