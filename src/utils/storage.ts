const localKey = process.env.REACT_APP_LOCAL_DB_KEY || "FALLBACK_KEY"

export const Storage = {

  get(itemName: string, optionalResponse: any = null): any {
    const item = localStorage.getItem(localKey + "-" + itemName);
    const numPattern = new RegExp(/^\d+$/);
    const jsonPattern = new RegExp(/[[{].*[}\]]/);

    if (item) {
      if (jsonPattern.test(item)) {
        return JSON.parse(item);
      } else if (numPattern.test(item)) {
        return parseFloat(item);
      } else {
        return item;
      }
    } else {
      return optionalResponse;
    }
  },

  set(itemName: string, item: any): void {
    if (typeof item === "object") {
      localStorage.setItem(
        localKey + "-" + itemName,
        JSON.stringify(item)
      );
    } else {
      localStorage.setItem(localKey + "-" + itemName, item);
    }
  },

  remove(itemName: string): void {
    localStorage.removeItem(localKey + "-" + itemName);
  }
}