import { posts } from "../data/data";

export const http = {
  get(): Promise<any> {
    return Promise.resolve(posts)
  }
}