export interface IFavorite {
  [key: string]: boolean;
}