import { IPost } from "./Post";

export interface IResponse {
  hits: IPost[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
}