export interface IPost {
  objectID: string;
  created_at: string;
  story_title: string;
  story_url: string;
  author: string;
}