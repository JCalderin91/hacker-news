
export const posts = {
  hits: [
    {
      created_at: "2022-06-17T20:34:43.000Z",
      title: null,
      url: null,
      author: "photochemsyn",
      points: null,
      story_text: null,
      comment_text:
        'If you\u0026#x27;re talking about computational modeling of chemical reactions, for example getting a computer to figure out a novel low-cost synthesis route for an important molecule, well... This becomes incredibly complicated very quickly.  It\u0026#x27;s generally more likely to get a result using the traditional experimental methods, with some exceptions for very small molecules perhaps.\u003cp\u003eThe field of physical inorganic\u0026#x2F;organic chemistry is one of the more difficult ones to build accurate models for.  A first step is to calculate the electronic structure of products, reactants, possible intermediaries, and this blows up fast for even moderately complex molecules.  A lot of work has been done with simpler systems like 2 H2O -\u0026gt; 2 H2 + O2 but even that\u0026#x27;s ridiculously complicated, as you have to model the catalyst and the surrounding environment as well, and then get the kinetic model right.  The computational power required is on the supercomputer scale, and the level of background knowledge required is pretty high to even start to implement something like that, for a taste see:\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;h2awsm.org\u0026#x2F;capabilities\u0026#x2F;dft-and-ab-initio-calculations-water-splitting-including-real-time-time-dependent" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;h2awsm.org\u0026#x2F;capabilities\u0026#x2F;dft-and-ab-initio-calculatio...\u003c/a\u003e\u003cp\u003eThis is an area where quantum computers may have applications (2021):\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;www.energy.gov\u0026#x2F;science\u0026#x2F;ascr\u0026#x2F;articles\u0026#x2F;quantum-computing-enables-unprecedented-materials-science-simulations" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.energy.gov\u0026#x2F;science\u0026#x2F;ascr\u0026#x2F;articles\u0026#x2F;quantum-computi...\u003c/a\u003e',
      num_comments: null,
      story_id: 31745934,
      story_title:
        "Making the collective knowledge of chemistry open and machine actionable",
      story_url: "https://www.nature.com/articles/s41557-022-00910-7",
      parent_id: 31781943,
      created_at_i: 1655498083,
      _tags: ["comment", "author_photochemsyn", "story_31745934"],
      objectID: "31783956",
      _highlightResult: {
        author: { value: "photochemsyn", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            'If you\'re talking about computational modeling of chemical \u003cem\u003ereactio\u003c/em\u003ens, for example getting a computer to figure out a novel low-cost synthesis route for an important molecule, well... This becomes incredibly complicated very quickly.  It\'s generally more likely to get a result using the traditional experimental methods, with some exceptions for very small molecules perhaps.\u003cp\u003eThe field of physical inorganic/organic chemistry is one of the more difficult ones to build accurate models for.  A first step is to calculate the electronic structure of products, reactants, possible intermediaries, and this blows up fast for even moderately complex molecules.  A lot of work has been done with simpler systems like 2 H2O -\u0026gt; 2 H2 + O2 but even that\'s ridiculously complicated, as you have to model the catalyst and the surrounding environment as well, and then get the kinetic model right.  The computational power required is on the supercomputer scale, and the level of background knowledge required is pretty high to even start to implement something like that, for a taste see:\u003cp\u003e\u003ca href="https://h2awsm.org/capabilities/dft-and-ab-initio-calculations-water-splitting-including-real-time-time-dependent" rel="nofollow"\u003ehttps://h2awsm.org/capabilities/dft-and-ab-initio-calculatio...\u003c/a\u003e\u003cp\u003eThis is an area where quantum computers may have applications (2021):\u003cp\u003e\u003ca href="https://www.energy.gov/science/ascr/articles/quantum-computing-enables-unprecedented-materials-science-simulations" rel="nofollow"\u003ehttps://www.energy.gov/science/ascr/articles/quantum-computi...\u003c/a\u003e',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Making the collective knowledge of chemistry open and machine actionable",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://www.nature.com/articles/s41557-022-00910-7",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T20:10:37.000Z",
      title: null,
      url: null,
      author: "jhgb",
      points: null,
      story_text: null,
      comment_text:
        "\u0026gt; There is a reason they put nuclear reactors into submarines and aircraft carriers and space probes.\u003cp\u003eThe first two have ready access to infinite amounts of coolant, which is absolutely not the situation \u0026quot;everywhere\u0026quot;, and the last one actually never delivered more power than solar panels due to very inferior power\u0026#x2F;weight ratio of all space-based nuclear reactors produced to this date -- the most widespread space-based reactor BES-5 generated something like 7-8 W\u0026#x2F;kg.",
      num_comments: null,
      story_id: 31750091,
      story_title: "US Army deploys its first floating solar array",
      story_url:
        "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
      parent_id: 31783299,
      created_at_i: 1655496637,
      _tags: ["comment", "author_jhgb", "story_31750091"],
      objectID: "31783620",
      _highlightResult: {
        author: { value: "jhgb", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "\u0026gt; There is a reason they put nuclear \u003cem\u003ereactors\u003c/em\u003e into submarines and aircraft carriers and space probes.\u003cp\u003eThe first two have ready access to infinite amounts of coolant, which is absolutely not the situation \u0026quot;everywhere\u0026quot;, and the last one actually never delivered more power than solar panels due to very inferior power/weight ratio of all space-based nuclear \u003cem\u003ereactors\u003c/em\u003e produced to this date -- the most widespread space-based reactor BES-5 generated something like 7-8 W/kg.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "US Army deploys its first floating solar array",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:59:37.000Z",
      title: null,
      url: null,
      author: "frakkingcylons",
      points: null,
      story_text: null,
      comment_text:
        "The point still stands: other employers are offering competitive wages for similar roles and are forcing Amazon to react. Otherwise Amazon wouldn’t be struggling to hire warehouse workers.",
      num_comments: null,
      story_id: 31778909,
      story_title:
        "Leaked Amazon memo warns the company is running out of people to hire",
      story_url:
        "https://www.vox.com/recode/23170900/leaked-amazon-memo-warehouses-hiring-shortage",
      parent_id: 31781679,
      created_at_i: 1655495977,
      _tags: ["comment", "author_frakkingcylons", "story_31778909"],
      objectID: "31783474",
      _highlightResult: {
        author: {
          value: "frakkingcylons",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "The point still stands: other employers are offering competitive wages for similar roles and are forcing Amazon to \u003cem\u003ereact\u003c/em\u003e. Otherwise Amazon wouldn’t be struggling to hire warehouse workers.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Leaked Amazon memo warns the company is running out of people to hire",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.vox.com/recode/23170900/leaked-amazon-memo-warehouses-hiring-shortage",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:45:39.000Z",
      title: null,
      url: null,
      author: "Sevii",
      points: null,
      story_text: null,
      comment_text:
        "There is a real problem where as humans we are really only looking for \u0026#x27;alien intelligence\u0026#x27; that we can interact with.\u003cp\u003eWe would have a hard time identifying a solar plasma based entity that had reaction times in years.\u003cp\u003eBut redefining \u0026#x27;intelligence\u0026#x27; to include all complex processes is not useful. I don\u0026#x27;t care if a storm in a gas giant is just as complex as a chimpanzee\u0026#x27;s thought processes.\u003cp\u003eA rabbit is intelligent compared to an ant, but we wouldn\u0026#x27;t consider discovering aliens at that level of intelligence relevant to us.\u003cp\u003eWhat we are looking for is near-peer intelligent life. Meaning about as intelligent as humans at timescales competitive to human cognition.",
      num_comments: null,
      story_id: 31781234,
      story_title: "Alien Intelligence and the Concept of Technology",
      story_url:
        "https://writings.stephenwolfram.com/2022/06/alien-intelligence-and-the-concept-of-technology/",
      parent_id: 31781234,
      created_at_i: 1655495139,
      _tags: ["comment", "author_Sevii", "story_31781234"],
      objectID: "31783317",
      _highlightResult: {
        author: { value: "Sevii", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "There is a real problem where as humans we are really only looking for 'alien intelligence' that we can interact with.\u003cp\u003eWe would have a hard time identifying a solar plasma based entity that had \u003cem\u003ereactio\u003c/em\u003en times in years.\u003cp\u003eBut redefining 'intelligence' to include all complex processes is not useful. I don't care if a storm in a gas giant is just as complex as a chimpanzee's thought processes.\u003cp\u003eA rabbit is intelligent compared to an ant, but we wouldn't consider discovering aliens at that level of intelligence relevant to us.\u003cp\u003eWhat we are looking for is near-peer intelligent life. Meaning about as intelligent as humans at timescales competitive to human cognition.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "Alien Intelligence and the Concept of Technology",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://writings.stephenwolfram.com/2022/06/alien-intelligence-and-the-concept-of-technology/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:43:50.000Z",
      title: null,
      url: null,
      author: "cbmuser",
      points: null,
      story_text: null,
      comment_text:
        "Yet, solar does not produce electricity reliably, needs additional electric backup power and uses large amounts of material and area for producing relatively small amounts of energy.\u003cp\u003eNuclear works everywhere, everytime. There is a reason they put nuclear reactors into submarines and aircraft carriers and space probes.",
      num_comments: null,
      story_id: 31750091,
      story_title: "US Army deploys its first floating solar array",
      story_url:
        "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
      parent_id: 31783179,
      created_at_i: 1655495030,
      _tags: ["comment", "author_cbmuser", "story_31750091"],
      objectID: "31783299",
      _highlightResult: {
        author: { value: "cbmuser", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "Yet, solar does not produce electricity reliably, needs additional electric backup power and uses large amounts of material and area for producing relatively small amounts of energy.\u003cp\u003eNuclear works everywhere, everytime. There is a reason they put nuclear \u003cem\u003ereactors\u003c/em\u003e into submarines and aircraft carriers and space probes.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "US Army deploys its first floating solar array",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:34:04.000Z",
      title: null,
      url: null,
      author: "Spooky23",
      points: null,
      story_text: null,
      comment_text:
        "Please. It’s deeply embedded in GOP power brokering. Principles are for the mass of idiots.\u003cp\u003eThe only difference today is that after years of taking the votes of reactionaries and doing little, the populist blocs of reactionary voters are the tail wagging the dog.",
      num_comments: null,
      story_id: 31778909,
      story_title:
        "Leaked Amazon memo warns the company is running out of people to hire",
      story_url:
        "https://www.vox.com/recode/23170900/leaked-amazon-memo-warehouses-hiring-shortage",
      parent_id: 31779734,
      created_at_i: 1655494444,
      _tags: ["comment", "author_Spooky23", "story_31778909"],
      objectID: "31783171",
      _highlightResult: {
        author: { value: "Spooky23", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "Please. It’s deeply embedded in GOP power brokering. Principles are for the mass of idiots.\u003cp\u003eThe only difference today is that after years of taking the votes of \u003cem\u003ereactio\u003c/em\u003enaries and doing little, the populist blocs of \u003cem\u003ereactio\u003c/em\u003enary voters are the tail wagging the dog.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Leaked Amazon memo warns the company is running out of people to hire",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.vox.com/recode/23170900/leaked-amazon-memo-warehouses-hiring-shortage",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:20:50.000Z",
      title: null,
      url: null,
      author: "stefan_",
      points: null,
      story_text: null,
      comment_text:
        "This is bizarre; are you just reciting random terms you picked up somewhere? Of course none of this reaches any level of \u0026quot;harassment\u0026quot; or \u0026quot;cyberbullying\u0026quot;.",
      num_comments: null,
      story_id: 31777613,
      story_title: "SpaceX fires employees involved in letter critical of Musk",
      story_url:
        "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
      parent_id: 31780486,
      created_at_i: 1655493650,
      _tags: ["comment", "author_stefan_", "story_31777613"],
      objectID: "31783022",
      _highlightResult: {
        author: { value: "stefan_", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "This is bizarre; are you just reciting random terms you picked up somewhere? Of course none of this \u003cem\u003ereaches\u003c/em\u003e any level of \u0026quot;harassment\u0026quot; or \u0026quot;cyberbullying\u0026quot;.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "SpaceX fires employees involved in letter critical of Musk",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:08:06.000Z",
      title: null,
      url: null,
      author: "statuslover9000",
      points: null,
      story_text: null,
      comment_text:
        'For chemical reaction prediction, see the Open Reaction Database, a collaboration including the Coley lab at MIT (surprisingly not cited by OP):\u003cp\u003ePaper: \u003ca href="https:\u0026#x2F;\u0026#x2F;pubs.acs.org\u0026#x2F;doi\u0026#x2F;10.1021\u0026#x2F;jacs.1c09820" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;pubs.acs.org\u0026#x2F;doi\u0026#x2F;10.1021\u0026#x2F;jacs.1c09820\u003c/a\u003e\u003cp\u003eDocs: \u003ca href="https:\u0026#x2F;\u0026#x2F;docs.open-reaction-database.org\u0026#x2F;en\u0026#x2F;latest\u0026#x2F;overview.html" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;docs.open-reaction-database.org\u0026#x2F;en\u0026#x2F;latest\u0026#x2F;overview.h...\u003c/a\u003e\u003cp\u003eIt’s an incredible effort to collate and clean this data, and even then a substantial portion of it will not be reproducible due to experimental variability or outright errors.\u003cp\u003eFor computational methods development it’s extremely useful, maybe even necessary, to have a substantial amount of money and one’s own lab space to collect new data and experimentally test prospective predictions under tightly controlled conditions. The historical data is certainly useful but is not a panacea.',
      num_comments: null,
      story_id: 31745934,
      story_title:
        "Making the collective knowledge of chemistry open and machine actionable",
      story_url: "https://www.nature.com/articles/s41557-022-00910-7",
      parent_id: 31745934,
      created_at_i: 1655492886,
      _tags: ["comment", "author_statuslover9000", "story_31745934"],
      objectID: "31782871",
      _highlightResult: {
        author: {
          value: "statuslover9000",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            'For chemical \u003cem\u003ereactio\u003c/em\u003en prediction, see the Open \u003cem\u003eReactio\u003c/em\u003en Database, a collaboration including the Coley lab at MIT (surprisingly not cited by OP):\u003cp\u003ePaper: \u003ca href="https://pubs.acs.org/doi/10.1021/jacs.1c09820" rel="nofollow"\u003ehttps://pubs.acs.org/doi/10.1021/jacs.1c09820\u003c/a\u003e\u003cp\u003eDocs: \u003ca href="https://docs.open-reaction-database.org/en/latest/overview.html" rel="nofollow"\u003ehttps://docs.open-\u003cem\u003ereactio\u003c/em\u003en-database.org/en/latest/overview.h...\u003c/a\u003e\u003cp\u003eIt’s an incredible effort to collate and clean this data, and even then a substantial portion of it will not be reproducible due to experimental variability or outright errors.\u003cp\u003eFor computational methods development it’s extremely useful, maybe even necessary, to have a substantial amount of money and one’s own lab space to collect new data and experimentally test prospective predictions under tightly controlled conditions. The historical data is certainly useful but is not a panacea.',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Making the collective knowledge of chemistry open and machine actionable",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://www.nature.com/articles/s41557-022-00910-7",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:05:49.000Z",
      title: null,
      url: null,
      author: "toyg",
      points: null,
      story_text: null,
      comment_text:
        "I mean, you started with \u0026quot;Boris Johnson is our only hope\u0026quot;, so I assumed HN etiquette was out of the window at that point.\u003cp\u003e\u003ci\u003e\u0026gt;  2 years better options will be available, but that\u0026#x27;s in 2 years time\u003c/i\u003e\u003cp\u003eYou assume the Tories can produce a majority after the current \u0026quot;Brexit coalition\u0026quot;, held together by Johnson, collapses. That\u0026#x27;s not a given.  It\u0026#x27;s also not a given that whichever cabinet a new PM could produce, will be strong enough to enact big policies.\u003cp\u003e\u003ci\u003e\u0026gt; but they don\u0026#x27;t seek to actively antagonise others.\u003c/i\u003e\u003cp\u003ePolicies like Rwanda deportations and the return of imperial measures are absolutely designed to produce outrage, and I challenge you to prove otherwise.\u003cp\u003eThe basic Johnson strategies are directly copied from the US playbook: they deliberately provoke the left in order to consolidate the right by defensive reaction, playing the victim and distracting from failures and scandals. And it works, for a while at least.\u003cp\u003eIs the entire party like that? No, but the people who are, are currently running the show.\u003cp\u003e\u003ci\u003e\u0026gt; they\u0026#x27;re a centre-right party that seeks a wide base\u003c/i\u003e\u003cp\u003eThey \u003ci\u003ewere\u003c/i\u003e. They stopped being that when absolute power went to the likes of Reese-Mogg. They attracted radical Northern votes by acting extremist on issues where the Labour party refuses to do. This is not your dad\u0026#x27;s Tory party.",
      num_comments: null,
      story_id: 31776895,
      story_title: "The bait-and-switch hidden in today’s cookie announcement",
      story_url:
        "https://webdevlaw.uk/2022/06/17/data-reform-bill-cookie-popups/",
      parent_id: 31780088,
      created_at_i: 1655492749,
      _tags: ["comment", "author_toyg", "story_31776895"],
      objectID: "31782841",
      _highlightResult: {
        author: { value: "toyg", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "I mean, you started with \u0026quot;Boris Johnson is our only hope\u0026quot;, so I assumed HN etiquette was out of the window at that point.\u003cp\u003e\u003ci\u003e\u0026gt;  2 years better options will be available, but that's in 2 years time\u003c/i\u003e\u003cp\u003eYou assume the Tories can produce a majority after the current \u0026quot;Brexit coalition\u0026quot;, held together by Johnson, collapses. That's not a given.  It's also not a given that whichever cabinet a new PM could produce, will be strong enough to enact big policies.\u003cp\u003e\u003ci\u003e\u0026gt; but they don't seek to actively antagonise others.\u003c/i\u003e\u003cp\u003ePolicies like Rwanda deportations and the return of imperial measures are absolutely designed to produce outrage, and I challenge you to prove otherwise.\u003cp\u003eThe basic Johnson strategies are directly copied from the US playbook: they deliberately provoke the left in order to consolidate the right by defensive \u003cem\u003ereactio\u003c/em\u003en, playing the victim and distracting from failures and scandals. And it works, for a while at least.\u003cp\u003eIs the entire party like that? No, but the people who are, are currently running the show.\u003cp\u003e\u003ci\u003e\u0026gt; they're a centre-right party that seeks a wide base\u003c/i\u003e\u003cp\u003eThey \u003ci\u003ewere\u003c/i\u003e. They stopped being that when absolute power went to the likes of Reese-Mogg. They attracted radical Northern votes by acting extremist on issues where the Labour party refuses to do. This is not your dad's Tory party.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "The bait-and-switch hidden in today’s cookie announcement",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://webdevlaw.uk/2022/06/17/data-reform-bill-cookie-popups/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T19:03:38.000Z",
      title: null,
      url: null,
      author: "mint2",
      points: null,
      story_text: null,
      comment_text:
        "Probably dealing with enough meta data to capture the stuff like the reaction only works because the supplier of one of the reagents used by that lab had ppm copper impurities",
      num_comments: null,
      story_id: 31745934,
      story_title:
        "Making the collective knowledge of chemistry open and machine actionable",
      story_url: "https://www.nature.com/articles/s41557-022-00910-7",
      parent_id: 31781283,
      created_at_i: 1655492618,
      _tags: ["comment", "author_mint2", "story_31745934"],
      objectID: "31782809",
      _highlightResult: {
        author: { value: "mint2", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "Probably dealing with enough meta data to capture the stuff like the \u003cem\u003ereactio\u003c/em\u003en only works because the supplier of one of the reagents used by that lab had ppm copper impurities",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Making the collective knowledge of chemistry open and machine actionable",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://www.nature.com/articles/s41557-022-00910-7",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T18:50:27.000Z",
      title: null,
      url: null,
      author: "wmeredith",
      points: null,
      story_text: null,
      comment_text:
        "I had the exact same reaction. This needs to be about 3 times faster in order for me to not to be annoyed by it, and that\u0026#x27;s why gamifying things that aren\u0026#x27;t games is usually obnoxious. If this is an interface for an actual game, meant to entertain, fine. If this was in some productivity software, it would annoy me at first, and then really piss me off after the 10th time I saw it or had to get past it to do what I want.\u003cp\u003eIt reminds of Apple\u0026#x27;s Siri and its grating cuteness in failure modes. Or of the Wordpress login box that gives me a cute shaking animation if the input is wrong.",
      num_comments: null,
      story_id: 31781110,
      story_title: "The Most Satisfying Checkbox",
      story_url: "https://www.andy.works/words/the-most-satisfying-checkbox",
      parent_id: 31782125,
      created_at_i: 1655491827,
      _tags: ["comment", "author_wmeredith", "story_31781110"],
      objectID: "31782641",
      _highlightResult: {
        author: { value: "wmeredith", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "I had the exact same \u003cem\u003ereactio\u003c/em\u003en. This needs to be about 3 times faster in order for me to not to be annoyed by it, and that's why gamifying things that aren't games is usually obnoxious. If this is an interface for an actual game, meant to entertain, fine. If this was in some productivity software, it would annoy me at first, and then really piss me off after the 10th time I saw it or had to get past it to do what I want.\u003cp\u003eIt reminds of Apple's Siri and its grating cuteness in failure modes. Or of the Wordpress login box that gives me a cute shaking animation if the input is wrong.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "The Most Satisfying Checkbox",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://www.andy.works/words/the-most-satisfying-checkbox",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T18:34:03.000Z",
      title: null,
      url: null,
      author: "zarmin",
      points: null,
      story_text: null,
      comment_text:
        'Rich Harris - Rethinking Reactivity: \u003ca href="https:\u0026#x2F;\u0026#x2F;www.youtube.com\u0026#x2F;watch?v=AdNJ3fydeao" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.youtube.com\u0026#x2F;watch?v=AdNJ3fydeao\u003c/a\u003e',
      num_comments: null,
      story_id: 31782200,
      story_title: "Ask HN: Best Dev Tool pitches of all time?",
      story_url: null,
      parent_id: 31782200,
      created_at_i: 1655490843,
      _tags: ["comment", "author_zarmin", "story_31782200"],
      objectID: "31782427",
      _highlightResult: {
        author: { value: "zarmin", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            'Rich Harris - Rethinking \u003cem\u003eReactiv\u003c/em\u003eity: \u003ca href="https://www.youtube.com/watch?v=AdNJ3fydeao" rel="nofollow"\u003ehttps://www.youtube.com/watch?v=AdNJ3fydeao\u003c/a\u003e',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "Ask HN: Best Dev Tool pitches of all time?",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T18:07:56.000Z",
      title: null,
      url: null,
      author: "AlexandrB",
      points: null,
      story_text: null,
      comment_text:
        "I can\u0026#x27;t even name another car company CEO. Musk brought this kind of reaction on Tesla by closely associating Tesla\u0026#x27;s brand with his own personal brand. This cuts both ways though and what was once a net positive is starting to turn negative.",
      num_comments: null,
      story_id: 31777613,
      story_title: "SpaceX fires employees involved in letter critical of Musk",
      story_url:
        "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
      parent_id: 31780454,
      created_at_i: 1655489276,
      _tags: ["comment", "author_AlexandrB", "story_31777613"],
      objectID: "31782034",
      _highlightResult: {
        author: { value: "AlexandrB", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "I can't even name another car company CEO. Musk brought this kind of \u003cem\u003ereactio\u003c/em\u003en on Tesla by closely associating Tesla's brand with his own personal brand. This cuts both ways though and what was once a net positive is starting to turn negative.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "SpaceX fires employees involved in letter critical of Musk",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T18:03:01.000Z",
      title: null,
      url: null,
      author: "WorldMaker",
      points: null,
      story_text: null,
      comment_text:
        'Have you tried importing the System.Interactive.Async package? [0] It\u0026#x27;s sometimes referred to as \u0026quot;Ix\u0026quot; (Interactive Extensions) to relate it to its older brother System.Reactive (ReactiveX or Rx). It adds a ton of LINQ-style extensions that make flowing between async and sync code easier. Despite the \u0026quot;System\u0026quot; name you always have to install it from NuGet and it\u0026#x27;s not just available out of the box, but it lights up so many LINQ extensions when installed and paired with a `using System.Linq.Async;`.\u003cp\u003eThough I also think IAsyncEnumerable is even better when paired with Rx, too, as some things are easier to express as Observable \u0026quot;in the middle\u0026quot; as an AsyncEnumerable-\u0026gt;Observable-\u0026gt;AsyncEnumerable \u0026quot;sandwich\u0026quot;. (You can sort of think of it as an pullable Event Source being spread into a push-based message bus\u0026#x2F;pipeline and then being collected back into a pullable Result feed.)\u003cp\u003e[0] \u003ca href="https:\u0026#x2F;\u0026#x2F;www.nuget.org\u0026#x2F;packages\u0026#x2F;System.Interactive.Async\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.nuget.org\u0026#x2F;packages\u0026#x2F;System.Interactive.Async\u0026#x2F;\u003c/a\u003e',
      num_comments: null,
      story_id: 31753608,
      story_title: "C#: IEnumerable, yield return, and lazy evaluation",
      story_url:
        "https://stackoverflow.blog/2022/06/15/c-ienumerable-yield-return-and-lazy-evaluation/",
      parent_id: 31776258,
      created_at_i: 1655488981,
      _tags: ["comment", "author_WorldMaker", "story_31753608"],
      objectID: "31781969",
      _highlightResult: {
        author: { value: "WorldMaker", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            'Have you tried importing the System.Interactive.Async package? [0] It\'s sometimes referred to as \u0026quot;Ix\u0026quot; (Interactive Extensions) to relate it to its older brother System.\u003cem\u003eReactiv\u003c/em\u003ee (\u003cem\u003eReactiv\u003c/em\u003eeX or Rx). It adds a ton of LINQ-style extensions that make flowing between async and sync code easier. Despite the \u0026quot;System\u0026quot; name you always have to install it from NuGet and it\'s not just available out of the box, but it lights up so many LINQ extensions when installed and paired with a `using System.Linq.Async;`.\u003cp\u003eThough I also think IAsyncEnumerable is even better when paired with Rx, too, as some things are easier to express as Observable \u0026quot;in the middle\u0026quot; as an AsyncEnumerable-\u0026gt;Observable-\u0026gt;AsyncEnumerable \u0026quot;sandwich\u0026quot;. (You can sort of think of it as an pullable Event Source being spread into a push-based message bus/pipeline and then being collected back into a pullable Result feed.)\u003cp\u003e[0] \u003ca href="https://www.nuget.org/packages/System.Interactive.Async/" rel="nofollow"\u003ehttps://www.nuget.org/packages/System.Interactive.Async/\u003c/a\u003e',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "C#: IEnumerable, yield return, and lazy evaluation",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://stackoverflow.blog/2022/06/15/c-ienumerable-yield-return-and-lazy-evaluation/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T18:00:56.000Z",
      title: null,
      url: null,
      author: "cellis",
      points: null,
      story_text: null,
      comment_text:
        "Can someone with more knowledge of Chemistry enlighten me why chemistry experimentation isn\u0026#x27;t the killer app for the Metaverse, at least for low-order reactions? I know the e.g. protein folding class of problems are prohibitively computationally expensive, but surely there\u0026#x27;s some low hanging fruit?",
      num_comments: null,
      story_id: 31745934,
      story_title:
        "Making the collective knowledge of chemistry open and machine actionable",
      story_url: "https://www.nature.com/articles/s41557-022-00910-7",
      parent_id: 31745934,
      created_at_i: 1655488856,
      _tags: ["comment", "author_cellis", "story_31745934"],
      objectID: "31781943",
      _highlightResult: {
        author: { value: "cellis", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "Can someone with more knowledge of Chemistry enlighten me why chemistry experimentation isn't the killer app for the Metaverse, at least for low-order \u003cem\u003ereactio\u003c/em\u003ens? I know the e.g. protein folding class of problems are prohibitively computationally expensive, but surely there's some low hanging fruit?",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Making the collective knowledge of chemistry open and machine actionable",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://www.nature.com/articles/s41557-022-00910-7",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T17:54:03.000Z",
      title: null,
      url: null,
      author: "bequanna",
      points: null,
      story_text: null,
      comment_text:
        "Only if you can also make energy storage super small and portable for those times when the sun isn’t shining.\u003cp\u003eSeems easier to invest in micro reactors.",
      num_comments: null,
      story_id: 31750091,
      story_title: "US Army deploys its first floating solar array",
      story_url:
        "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
      parent_id: 31779224,
      created_at_i: 1655488443,
      _tags: ["comment", "author_bequanna", "story_31750091"],
      objectID: "31781838",
      _highlightResult: {
        author: { value: "bequanna", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "Only if you can also make energy storage super small and portable for those times when the sun isn’t shining.\u003cp\u003eSeems easier to invest in micro \u003cem\u003ereactors\u003c/em\u003e.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "US Army deploys its first floating solar array",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.theverge.com/2022/6/14/23167441/us-army-floating-solar-power-plant-floatovoltaics",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T17:54:03.000Z",
      title: null,
      url: null,
      author: "throwaway0a5e",
      points: null,
      story_text: null,
      comment_text:
        "There are two points I was getting at with my heuristic.\u003cp\u003eI primarily wanted to highlight that the people quipping about \u0026quot;there\u0026#x27;s no far left in the US\u0026quot; or \u0026quot;the far left in the US is the mainstream left in Europe\u0026quot; are far left hence why they don\u0026#x27;t see much to the left of them in the US.  Left-right, tall-short, skinny-fat, if you find yourself on a spectrum and one side of you is sparsely populated you are the extreme by definition.\u003cp\u003eSecond, these people are generally ignorant of how far right some of the \u0026quot;other half\u0026quot; of Europe leans on social issues.  Sure, they have lots of government services and safety nets, healthcare included.  But they don\u0026#x27;t lean as far left on many social subjects as the US does.  Identity politics, sexual orientation and abortions are three good examples of topics on which the left half of the bell curve of opinions on these subjects has more of it\u0026#x27;s meat to the right than the US equivalent.  (Arguably a lot of this is a function of the US\u0026#x27;s history of reactionary politics but that\u0026#x27;s outside of the scope of this discussion, the positions today are what they are.)  So even in a context that includes \u0026quot;nations who literally tried communism and kept the stuff they liked when they were done\u0026quot; the opinions of the \u0026quot;far\u0026quot; left in the US is decently out there.",
      num_comments: null,
      story_id: 31779105,
      story_title:
        "SpaceX fires employees behind letter slamming Musk, tells staff: “Stay focused”",
      story_url:
        "https://arstechnica.com/tech-policy/2022/06/spacex-fired-employees-who-wrote-letter-slamming-musks-embarrassing-behavior/",
      parent_id: 31781515,
      created_at_i: 1655488443,
      _tags: ["comment", "author_throwaway0a5e", "story_31779105"],
      objectID: "31781837",
      _highlightResult: {
        author: {
          value: "throwaway0a5e",
          matchLevel: "none",
          matchedWords: [],
        },
        comment_text: {
          value:
            "There are two points I was getting at with my heuristic.\u003cp\u003eI primarily wanted to highlight that the people quipping about \u0026quot;there's no far left in the US\u0026quot; or \u0026quot;the far left in the US is the mainstream left in Europe\u0026quot; are far left hence why they don't see much to the left of them in the US.  Left-right, tall-short, skinny-fat, if you find yourself on a spectrum and one side of you is sparsely populated you are the extreme by definition.\u003cp\u003eSecond, these people are generally ignorant of how far right some of the \u0026quot;other half\u0026quot; of Europe leans on social issues.  Sure, they have lots of government services and safety nets, healthcare included.  But they don't lean as far left on many social subjects as the US does.  Identity politics, sexual orientation and abortions are three good examples of topics on which the left half of the bell curve of opinions on these subjects has more of it's meat to the right than the US equivalent.  (Arguably a lot of this is a function of the US's history of \u003cem\u003ereactio\u003c/em\u003enary politics but that's outside of the scope of this discussion, the positions today are what they are.)  So even in a context that includes \u0026quot;nations who literally tried communism and kept the stuff they liked when they were done\u0026quot; the opinions of the \u0026quot;far\u0026quot; left in the US is decently out there.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "SpaceX fires employees behind letter slamming Musk, tells staff: “Stay focused”",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://arstechnica.com/tech-policy/2022/06/spacex-fired-employees-who-wrote-letter-slamming-musks-embarrassing-behavior/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T17:52:26.000Z",
      title: null,
      url: null,
      author: "jtode",
      points: null,
      story_text: null,
      comment_text:
        'That list is extremely incomplete, and makes it seem like bank runs are happening more lately than ever before. In the 19th century and before they were frequent:\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;www.federalreservehistory.org\u0026#x2F;essays\u0026#x2F;banking-panics-of-the-gilded-age" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.federalreservehistory.org\u0026#x2F;essays\u0026#x2F;banking-panics-...\u003c/a\u003e\u003cp\u003eAnd the creation of the federal reserve was in reaction to this.\u003cp\u003eOur contemporary love of deregulation is the most regressive and self-destructive push back into the bad old days I have ever witnessed. Texas, with their blackouts and water shutdowns, are showing America their future, and about half of the country seems to think that\u0026#x27;s a coming paradise.\u003cp\u003eI stopped trying to understand it years ago, for the most part. Other than through the facile P.T. Barnum framework, which I don\u0026#x27;t consider constructive or useful, as much as it seems to be true.',
      num_comments: null,
      story_id: 31776372,
      story_title:
        "Crypto lending company Babel Finance halts redemptions and withdrawals",
      story_url: "https://babel.finance/article-views.html?id=50",
      parent_id: 31780320,
      created_at_i: 1655488346,
      _tags: ["comment", "author_jtode", "story_31776372"],
      objectID: "31781814",
      _highlightResult: {
        author: { value: "jtode", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            'That list is extremely incomplete, and makes it seem like bank runs are happening more lately than ever before. In the 19th century and before they were frequent:\u003cp\u003e\u003ca href="https://www.federalreservehistory.org/essays/banking-panics-of-the-gilded-age" rel="nofollow"\u003ehttps://www.federalreservehistory.org/essays/banking-panics-...\u003c/a\u003e\u003cp\u003eAnd the creation of the federal reserve was in \u003cem\u003ereactio\u003c/em\u003en to this.\u003cp\u003eOur contemporary love of deregulation is the most regressive and self-destructive push back into the bad old days I have ever witnessed. Texas, with their blackouts and water shutdowns, are showing America their future, and about half of the country seems to think that\'s a coming paradise.\u003cp\u003eI stopped trying to understand it years ago, for the most part. Other than through the facile P.T. Barnum framework, which I don\'t consider constructive or useful, as much as it seems to be true.',
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value:
            "Crypto lending company Babel Finance halts redemptions and withdrawals",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value: "https://babel.finance/article-views.html?id=50",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T17:47:52.000Z",
      title: null,
      url: null,
      author: "WorldMaker",
      points: null,
      story_text: null,
      comment_text:
        "\u0026gt; If you had to start over, would you still recommend to yourself to learn F# in 2022?\u003cp\u003eLearn it, absolutely. A lot of comments here are arguing \u0026quot;maybe don\u0026#x27;t use it in Production\u0026quot;, but I think it\u0026#x27;s a clear case that it is still worth learning even if you aren\u0026#x27;t \u0026quot;using\u0026quot;.\u003cp\u003eEven though C# keeps adding a lot of F#-inspired features, there\u0026#x27;s still (and likely will always be) a large gap in the \u0026quot;native\u0026quot; idioms and the cultural best practices. There\u0026#x27;s still some big differences in encountering the \u0026quot;F#-like\u0026quot; stuff in the imperative wilds of C# than in the tame functional fields of F#. Learning those same tools in the \u0026quot;safer environment\u0026quot; where they feel more natural, learning the idioms that make them truly powerful in F# unlocks ways to think about these tools, that even if you aren\u0026#x27;t using them day to day \u003ci\u003ein\u003c/i\u003e F# and are just applying them to C#\u0026#x27;s relatives can still be very useful ways to think.\u003cp\u003e(A lot of what this article here talks about has a different perspective from F# and especially Haskell where \u0026quot;Lazy evaluation\u0026quot; is a much, more natural way of working than the imperative mindset of \u0026quot;every line does a thing immediately\u0026quot;, which is what imperative means.)\u003cp\u003eCoincidentally, I\u0026#x27;ve made several recommendations in recent PRs that developers take some time in F# lately. I really do strongly believe that learning it makes for better C# programmers in 2022, \u003ci\u003eespecially\u003c/i\u003e because of all the F# tools now in the language. It\u0026#x27;s too easy to fall into mental traps you don\u0026#x27;t even know might exist in LINQ and records and async\u0026#x2F;await and AsyncEnumerable and ReactiveX and so forth, with the list still growing, without the education of languages like F#. Even if the other comments can give you a lot of reasons why you maybe shouldn\u0026#x27;t push to use F# in Production, it\u0026#x27;s still an incredibly useful educational tool at least.",
      num_comments: null,
      story_id: 31753608,
      story_title: "C#: IEnumerable, yield return, and lazy evaluation",
      story_url:
        "https://stackoverflow.blog/2022/06/15/c-ienumerable-yield-return-and-lazy-evaluation/",
      parent_id: 31775167,
      created_at_i: 1655488072,
      _tags: ["comment", "author_WorldMaker", "story_31753608"],
      objectID: "31781742",
      _highlightResult: {
        author: { value: "WorldMaker", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "\u0026gt; If you had to start over, would you still recommend to yourself to learn F# in 2022?\u003cp\u003eLearn it, absolutely. A lot of comments here are arguing \u0026quot;maybe don't use it in Production\u0026quot;, but I think it's a clear case that it is still worth learning even if you aren't \u0026quot;using\u0026quot;.\u003cp\u003eEven though C# keeps adding a lot of F#-inspired features, there's still (and likely will always be) a large gap in the \u0026quot;native\u0026quot; idioms and the cultural best practices. There's still some big differences in encountering the \u0026quot;F#-like\u0026quot; stuff in the imperative wilds of C# than in the tame functional fields of F#. Learning those same tools in the \u0026quot;safer environment\u0026quot; where they feel more natural, learning the idioms that make them truly powerful in F# unlocks ways to think about these tools, that even if you aren't using them day to day \u003ci\u003ein\u003c/i\u003e F# and are just applying them to C#'s relatives can still be very useful ways to think.\u003cp\u003e(A lot of what this article here talks about has a different perspective from F# and especially Haskell where \u0026quot;Lazy evaluation\u0026quot; is a much, more natural way of working than the imperative mindset of \u0026quot;every line does a thing immediately\u0026quot;, which is what imperative means.)\u003cp\u003eCoincidentally, I've made several recommendations in recent PRs that developers take some time in F# lately. I really do strongly believe that learning it makes for better C# programmers in 2022, \u003ci\u003eespecially\u003c/i\u003e because of all the F# tools now in the language. It's too easy to fall into mental traps you don't even know might exist in LINQ and records and async/await and AsyncEnumerable and \u003cem\u003eReactiv\u003c/em\u003eeX and so forth, with the list still growing, without the education of languages like F#. Even if the other comments can give you a lot of reasons why you maybe shouldn't push to use F# in Production, it's still an incredibly useful educational tool at least.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "C#: IEnumerable, yield return, and lazy evaluation",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://stackoverflow.blog/2022/06/15/c-ienumerable-yield-return-and-lazy-evaluation/",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
    {
      created_at: "2022-06-17T17:36:01.000Z",
      title: null,
      url: null,
      author: "teawrecks",
      points: null,
      story_text: null,
      comment_text:
        "I think you\u0026#x27;re arguing tangentially to the point being made, which is that: no insubordination happened. They were simply critical of how Musk represented them.\u003cp\u003eI have to assume they knew when they penned the letter that they would find out whether their leader could take criticism and help them make a better company and product together, or react immaturely and let them know that their time would be better spent elsewhere. Seems they got their answer.\u003cp\u003eIn any case, yeah, Musk owns the company and has the right to fire people for criticizing his business decisions. Bold strategy, we\u0026#x27;ll see how it turns out for him.",
      num_comments: null,
      story_id: 31777613,
      story_title: "SpaceX fires employees involved in letter critical of Musk",
      story_url:
        "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
      parent_id: 31781079,
      created_at_i: 1655487361,
      _tags: ["comment", "author_teawrecks", "story_31777613"],
      objectID: "31781555",
      _highlightResult: {
        author: { value: "teawrecks", matchLevel: "none", matchedWords: [] },
        comment_text: {
          value:
            "I think you're arguing tangentially to the point being made, which is that: no insubordination happened. They were simply critical of how Musk represented them.\u003cp\u003eI have to assume they knew when they penned the letter that they would find out whether their leader could take criticism and help them make a better company and product together, or \u003cem\u003ereact\u003c/em\u003e immaturely and let them know that their time would be better spent elsewhere. Seems they got their answer.\u003cp\u003eIn any case, yeah, Musk owns the company and has the right to fire people for criticizing his business decisions. Bold strategy, we'll see how it turns out for him.",
          matchLevel: "full",
          fullyHighlighted: false,
          matchedWords: ["reactjs"],
        },
        story_title: {
          value: "SpaceX fires employees involved in letter critical of Musk",
          matchLevel: "none",
          matchedWords: [],
        },
        story_url: {
          value:
            "https://www.wsj.com/articles/spacex-fires-employees-involved-in-letter-critical-of-musk-company-11655471021",
          matchLevel: "none",
          matchedWords: [],
        },
      },
    },
  ],
  nbHits: 5859,
  page: 0,
  nbPages: 50,
  hitsPerPage: 20,
  exhaustiveNbHits: true,
  exhaustiveTypo: true,
  query: "reactjs",
  params:
    "advancedSyntax=true\u0026analytics=true\u0026analyticsTags=backend\u0026page=0\u0026query=reactjs",
  processingTimeMS: 11,
};
