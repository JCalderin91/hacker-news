import { IFramawork } from '../interfaces/Framework'
const angularLogo = require('../assets/images/framworks-icons/angular.png')
const reactLogo = require('../assets/images/framworks-icons/react.png')
const vueLogo = require('../assets/images/framworks-icons/vue.png')


const frameworks: IFramawork[] = [
  { value: 'angular', label: 'Angular', image: angularLogo },
  { value: 'reactjs', label: 'React', image: reactLogo },
  { value: 'vuejs', label: 'Vue', image: vueLogo }
]
export default frameworks

