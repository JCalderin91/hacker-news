import { useLayoutEffect, useState } from 'react'

export const useResize = ({ maxWidth = 800 }) => {
  const [offsetPages, setOffsetPages] = useState<number>(4)
  useLayoutEffect(() => {
    function updateSize() {
      if (window.innerWidth <= maxWidth) setOffsetPages(2)
      else setOffsetPages(4)
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, [maxWidth]);
  return { offsetPages }
}