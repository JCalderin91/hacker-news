import { IFavorite } from "../interfaces/Favorite";
import { IFramawork } from "../interfaces/Framework";
import { IPost } from "../interfaces/Post";
import { IResponse } from "../interfaces/Response";
import { useEffect, useState } from "react";
import frameworks from "../data/frameworks";
import { postsServices } from "../services/postsServices";
import { Storage } from "../utils/storage";

export const usePosts = () => {
  const [posts, setPosts] = useState<IPost[]>([]);
  const [page, setPage] = useState<number>(0);
  const [pageCount, setPageCount] = useState<number>(0);

  const [isLoading, setLoading] = useState<boolean>(true);
  const [filterAll, setFilterAll] = useState<boolean>(true);
  const [favorites, setFavorites] = useState<IFavorite>(Storage.get("favorites", {}));
  const [query, setQuery] = useState<IFramawork>(Storage.get("filter", frameworks[0]));

  const baseUrl = process.env.REACT_APP_URL_BASE;

  useEffect(() => {
    setLoading(true)
    postsServices
      .get(`${baseUrl}/search_by_date?query=${query.value}&page=${page}`)
      .then((response: IResponse) => {
        const posts: IPost[] = response.hits
          .filter(
            ({ author, created_at, story_title, story_url }) =>
              !!story_title && !!story_url && !!created_at && !!author
          )
          .map(({ story_url, story_title, created_at, author, objectID }) => ({
            story_url,
            story_title,
            created_at,
            author,
            objectID,
          }));
        setPosts(posts);
        setPageCount(response.nbPages);
      })
      .finally(() => {
        setLoading(false)
      })
  }, [query, page, baseUrl]);

  const handleFavorite = (postId: number | string): any => {
    const newFavotites = {
      ...favorites,
      [postId]: Boolean(favorites[postId]) ? false : true,
    };
    Storage.set("favorites", newFavotites);
    setFavorites(newFavotites);
  };
  const toogleFilter = (filterValue: boolean) => {
    setFilterAll(filterValue);
  };
  const changeTypeFilter = (value: IFramawork) => {
    setPage(0);
    Storage.set("filter", value);
    setQuery(value);
  };

  const handleChangePage = (newPage: number): void => {
    setPage(newPage);
  };

  return {
    isLoading,
    posts,
    pageCount,
    filterAll,
    favorites,
    page,
    query,
    handleFavorite,
    toogleFilter,
    changeTypeFilter,
    handleChangePage
  }
}