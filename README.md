# Hacker News

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) 

Demo: [Hacker News](https://hacker-news-reign.netlify.app/)

[![Netlify Status](https://api.netlify.com/api/v1/badges/e68fb5d1-26de-438a-bd8d-894ac3661003/deploy-status)](https://app.netlify.com/sites/hacker-news-reign/deploys)

Demo: [Hacker News - Infinity Scroll](https://hacker-news-infinity-scroll-reign.netlify.app/)

[![Netlify Status](https://api.netlify.com/api/v1/badges/66a221b1-fbd5-4631-8390-3457ca383ba0/deploy-status)](https://app.netlify.com/sites/hacker-news-infinity-scroll-reign/deploys)

## Stack

- Create React App
- SASS
- TypeScript
- Jest
- React Testing Library
- Timeago React
- React Select


Quick Start
-----------

```shell
$ git clone https://gitlab.com/JCalderin91/hacker-news.git
$ cd hacker-news
$ npm install
$ npm start
```

## NPM Commands

|Script|Description|
|---|---|
|`npm start`|Start webpack development server `localhost:3000`|
|`npm run build`|Build the application to `./build` directory|
|`npm test`|Test the application; watch for changes and retest|